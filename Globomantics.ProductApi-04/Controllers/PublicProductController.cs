﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Globomantics.ProductApi_04.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Globomantics.ProductApi_04.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("PublicApi")]
    public class PublicProductController : ControllerBase
    {
        [HttpGet("[action]")]
        public IEnumerable<Product> GetAllProducts()
        {
            var products = new Faker<Product>()
                .RuleFor(o => o.Id, f => Guid.NewGuid())
                .RuleFor(o => o.Name, f => f.Commerce.ProductName())
                .RuleFor(o => o.Price, f => f.Commerce.Price())
                .RuleFor(o => o.Image, f => f.Image.PicsumUrl(200, 200));

            return products.Generate(100).ToList();
        }
    }
}