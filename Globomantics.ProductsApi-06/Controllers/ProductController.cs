﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Bogus;
using Globomantics.ProductApi_06.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Globomantics.ProductApi_04.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        [HttpGet("[action]")]
        public async Task<IActionResult> GetAllProducts()
        {
            await SetPaginationHeader(10, 1, 100, 1000);
            var data = await GetProductsAsync();
            return Ok(data);
        }   


        private async Task<IEnumerable<Product>> GetProductsAsync()
        {
            var products = new Faker<Product>()
                .RuleFor(o => o.Id, f=>Guid.NewGuid())
                .RuleFor(o=>o.Name, f=>f.Commerce.ProductName())
                .RuleFor(o=>o.Price, f=>f.Commerce.Price())
                .RuleFor(o=>o.Image, f=>f.Image.PicsumUrl(200,200));

            return products.Generate(100).ToList();
        }

        private async Task SetPaginationHeader(int pageSize, int pageNo, int pageCount, int totalRecord)
        {
            HttpContext.Response.Headers.Add("PageNo", pageNo.ToString());
            HttpContext.Response.Headers.Add("PageSize", pageSize.ToString());
            HttpContext.Response.Headers.Add("PageCount", pageCount.ToString());
            HttpContext.Response.Headers.Add("PageTotalRecords", totalRecord.ToString());
        }
    }
}