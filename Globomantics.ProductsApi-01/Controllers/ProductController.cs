﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Threading.Tasks;
using Globomantics.ProductsApi_01.Models;
using Bogus;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Globomantics.ProductsApi_01.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        [HttpGet("[action]")]
        public IEnumerable<Product> GetAllProducts()
        {
            var products = new Faker<Product>()
                .RuleFor(o => o.Id, f => Guid.NewGuid())
                .RuleFor(o => o.Name, f => f.Commerce.ProductName())
                .RuleFor(o => o.Price, f => f.Commerce.Price())
                .RuleFor(o => o.Image, f => f.Image.PicsumUrl(200, 200));

            return products.Generate(100);
        }
    }
}